export const BONUS_AMMO = "ammo";


export default class BonusAmmo extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-ammo');

        this.AdditionalAmmo = 10;
        this.Type = BONUS_AMMO;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}