export const BONUS_JUMP = "jump";


export default class BonusJump extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-jump');

        this.AditionalJump = -50;

        this.Type = BONUS_JUMP;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}