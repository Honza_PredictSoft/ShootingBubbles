export const BONUS_HP = "HP";


export default class BonusHP extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-HP');

        this.AditionalHP = 50;

        this.Type = BONUS_HP;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}