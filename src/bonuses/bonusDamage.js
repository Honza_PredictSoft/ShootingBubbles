export const BONUS_DAMAGE = "damage";


export default class BonusDamage extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-damage');

        this.AdditionalDamage = 10;

        this.Type = BONUS_DAMAGE;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}