export const BONUS_PISTOL = "Pistol";


export default class BonusPistol extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-pistol');

        this.Type = BONUS_PISTOL;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}