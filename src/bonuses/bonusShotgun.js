export const BONUS_SHOTGUN = "Shotgun";


export default class BonusShotgun extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-shotgun');

        this.Type = BONUS_SHOTGUN;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}