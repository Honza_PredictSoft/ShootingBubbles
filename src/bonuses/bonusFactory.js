import BonusAmmo from './bonusAmmo.js';
import BonusDamage from './bonusDamage.js';
import BonusHP from './bonusHP.js';
import BonusJump from './bonusJump.js';
import BonusSpeed from './bonusSpeed.js';
import BonusPistol from './bonusPistol.js';
import BonusShotgun from './bonusShotgun.js';

const REFRESH_TIMEOUT = 30 * 1000;  // 30 seconds until new bonus is generated

export default class BonusFactory {
    constructor(scene, colliders, players, positionX, positionY) {
        this.scene = scene;
        this.colliders = colliders;
        this.players = players;
        this.x = positionX;
        this.y = positionY;

        this.bonuses = [BonusAmmo, BonusDamage, BonusHP, BonusJump, BonusSpeed, BonusPistol, BonusShotgun];
        this.generateBonus();
    }

    collectBonus(player, bonus) {
        player.Collect(bonus);
        bonus.destroy(true);

        setTimeout(this.generateBonus, REFRESH_TIMEOUT);
    }

    generateBonus = () => {
        const randomIndex = Math.floor(Math.random() * this.bonuses.length);
        var bonus = new this.bonuses[randomIndex](this.scene, this.x, this.y);

        this.scene.physics.add.collider(this.colliders, bonus);
        this.players.forEach(player => {
            this.scene.physics.add.overlap(player, bonus, () => this.collectBonus(player, bonus), null, this);
        }, this);
    }
}