export const BONUS_SPEED = "speed";


export default class BonusSpeed extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bonus-speed');

        this.AditionalSpeed = 100;

        this.Type = BONUS_SPEED;
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(1);
    }
}