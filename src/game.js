import Phaser from 'phaser';
import Player from './player.js';
import BonusFactory from './bonuses/bonusFactory.js';
import { VIEW_SIDE } from './player.js';
import { showRestartButton } from './index';


const BACKGROUND_COLOR = '#89E3FF';


var gameConfig;
var game;
var platforms;
var player1, player2;
var player1Info, player2Info;     // player2 helth status, current weapon...
var maxPlayerDistance;  // Players cant be too far from each other - shared screen
var camera;
var gameOver;

export default function StartNewGame(config) {
    gameConfig = config;
    var config = {
        type: Phaser.AUTO,
        width: config.screenSize.width,
        height: config.screenSize.height,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: config.gravity },
                debug: false
            }
        },
        scene: {
            preload: preload,
            create: create,
            update: update
        }
    };

    game = new Phaser.Game(config);
    return game;
}

function preload() {
    this.load.path = "./src/assets/images/";
    this.load.image('blank', 'blank.png');
    this.load.image('bullet', 'bullet.png');
    this.load.image('bonus-ammo', 'bonus-ammo-pack.png');
    this.load.image('bonus-damage', 'bonus-damage.png');
    this.load.image('bonus-HP', 'bonus-HP.png');
    this.load.image('bonus-jump', 'bonus-jump.png');
    this.load.image('bonus-speed', 'bonus-speed.png');
    this.load.image('bonus-pistol', 'bonus-pistol.png');
    this.load.image('bonus-shotgun', 'bonus-shotgun.png');
    this.load.image('ground', 'ground-100.png');
    this.load.image('platform', 'platform-100.png');
    this.load.image('platform-left', 'platform-left-100.png');
    this.load.image('platform-right', 'platform-right-100.png');
    this.load.spritesheet('dude', 'dude.png', { frameWidth: 32, frameHeight: 32 });

    this.load.path = "./src/assets/audio/";
    this.load.audio('pistol-sound', 'pistol-sound.mp3');
    this.load.audio('shotgun-sound', 'shotgun-sound.mp3');
    this.load.audio('damage-taken', 'damage-taken.mp3');
    this.load.audio('background-music', 'background-music.mp3');
    this.load.audio('game-over', 'game-over.wav');
}

function create() {
    gameOver = false;
    const restartButton = document.getElementById("restart-game-button");
    restartButton.addEventListener('click', (event) => {
        this.backgroundMusic.stop();
        this.scene.restart();
    });

    const PLATFORM_SIZE = {     // img size is 100x25 for horisontal and 25x100 for vertical platform
        BIG: 100,
        SMALL: 25
    }
    const height = gameConfig.screenSize.height;
    const width = gameConfig.worldWidth;
    maxPlayerDistance = gameConfig.screenSize.width - 200;

    this.physics.world.setBounds(0, 0, width, gameConfig.screenSize.height);

    // create world platforms
    platforms = this.physics.add.staticGroup();
    platforms.create(PLATFORM_SIZE.SMALL / 2, height / 2, 'platform-right').setScale(1, height / PLATFORM_SIZE.BIG).refreshBody();   // left world border
    platforms.create(width - PLATFORM_SIZE.SMALL / 2, height / 2, 'platform-left').setScale(1, height / PLATFORM_SIZE.BIG).refreshBody();    // right world border
    platforms.create(width / 2, height - PLATFORM_SIZE.SMALL / 2, 'ground').setScale(width / PLATFORM_SIZE.BIG, 1).refreshBody();    // world ground

    platforms.create(200 + PLATFORM_SIZE.BIG / 2, height - 100, 'platform');     // left horisontal platform
    platforms.create(200 + PLATFORM_SIZE.BIG + PLATFORM_SIZE.SMALL / 2, height - PLATFORM_SIZE.BIG, 'platform-right');   // left vertical blocker
    platforms.create(PLATFORM_SIZE.BIG / 2 + PLATFORM_SIZE.SMALL, height - 200, 'platform'); // left horisontal platform
    platforms.create(width / 2, height - 300, 'platform').setScale(2, 1).refreshBody();   // central horisontal platform
    platforms.create(width / 2 - PLATFORM_SIZE.SMALL / 2, height - PLATFORM_SIZE.SMALL - PLATFORM_SIZE.BIG / 2, 'platform-left');    // central vertical platform
    platforms.create(width / 2 + PLATFORM_SIZE.SMALL / 2, height - PLATFORM_SIZE.SMALL - PLATFORM_SIZE.BIG / 2, 'platform-right');   // central vertical platform
    platforms.create(width - 200 - PLATFORM_SIZE.BIG / 2, height - 100, 'platform'); // right horisontal platform
    platforms.create(width - 200 - PLATFORM_SIZE.BIG - PLATFORM_SIZE.SMALL / 2, height - PLATFORM_SIZE.BIG, 'platform-left');    // right vercital blocker
    platforms.create(width - PLATFORM_SIZE.BIG / 2 - PLATFORM_SIZE.SMALL, height - 200, 'platform'); // right horisontal platform

    // camera should stay in the middle of world window
    camera = this.add.image(width / 2, height / 2, 'blank');
    this.cameras.main.startFollow(camera);
    this.cameras.main.setBackgroundColor(BACKGROUND_COLOR);


    // initialize Player 1
    player1 = new Player(this, gameConfig.player1, VIEW_SIDE.RIGHT, 100 + width / 2 - gameConfig.screenSize.width / 2, height - 200);
    player1.setTint(0x22ff22);
    const player1InfoText = player1.GetInfoText();
    const player1InfoPosition = getLeftInfoPosition();
    player1Info = this.add.text(player1InfoPosition.x, player1InfoPosition.y, player1InfoText);

    // initialize Player 2
    player2 = new Player(this, gameConfig.player2, VIEW_SIDE.LEFT, width / 2 + gameConfig.screenSize.width / 2 - 100, height - 200);
    player2.setTint(0x2222ff);
    const player2InfoText = player2.GetInfoText();
    const player2InfoPosition = getRightInfoPosition();
    player2Info = this.add.text(player2InfoPosition.x, player2InfoPosition.y, player2InfoText);

    player1.AddColliders([player2, platforms]);
    player2.AddColliders([player1, platforms]);

    // add bonus places
    new BonusFactory(this, platforms, [player1, player2], width / 2, 100);
    new BonusFactory(this, platforms, [player1, player2], width / 2, 350);
    new BonusFactory(this, platforms, [player1, player2], 75, 300);
    new BonusFactory(this, platforms, [player1, player2], width - 75, 300);
 
    this.anims.create({
        key: 'left-move',
        frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'left-look',
        frames: [ { key: 'dude', frame: 0 } ],
        frameRate: 20
    });

    this.anims.create({
        key: 'right-move',
        frames: this.anims.generateFrameNumbers('dude', { start: 4, end: 7 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'right-look',
        frames: [ { key: 'dude', frame: 4 } ],
        frameRate: 20
    });

    this.gameOverSound = this.sound.add('game-over');
    this.backgroundMusic = this.sound.add('background-music');
    this.backgroundMusic.loop = true;
    this.backgroundMusic.play();
}

function update() {
    if (gameOver) {
        return;
    }

    const distance = Math.abs(player2.x - player1.x);
    if (distance >= maxPlayerDistance) {
        if (player2.x > player1.x) {
            // p1 ------------- p2
            player1.body.blocked.left = true;   // avoid player1 to go left
            player2.body.blocked.right = true;  // avoid player2 to go right
        } else {
            // p2 ------------- p1
            player1.body.blocked.right = true;  // avoid player1 to go right
            player2.body.blocked.left = true;   // avoid player2 to go left
        }
    }

    // check user controls
    player1.HandleControls();
    player2.HandleControls();


    // set the camera and info tables position
    camera.x = (player1.x + player2.x) / 2;
    player1Info.setText(player1.GetInfoText());
    player1Info.x = getLeftInfoPosition().x;
    player2Info.setText(player2.GetInfoText());
    player2Info.x = getRightInfoPosition().x;

    // check players health
    if (player1.Health === 0) {
        finishGame.call(this, player2);
    }
    if (player2.Health === 0) {
        finishGame.call(this, player1);
    }
}

function getLeftInfoPosition() {
    return {
        x: camera.x - gameConfig.screenSize.width / 2 + 20,
        y: 20
    };
}

function getRightInfoPosition() {
    return {
        x: camera.x + gameConfig.screenSize.width / 2 - 200,
        y: 20
    };
}

function finishGame(winner) {
    this.backgroundMusic.stop();
    this.gameOverSound.play();
    
    winner.setBounce(1);
    winner.setVelocityY(-300);

    var text = this.add.text(camera.x, camera.y, `${winner.name} is the WINNER`, {
        font: "bold 50px Arial",
        fill: '#000'
    });
    text.x -= text.width / 2;
    text.y -= text.height / 2;

    gameOver = true;
    showRestartButton();
}