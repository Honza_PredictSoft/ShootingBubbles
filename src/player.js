import Pistol from './weapons/pistol.js';
import { PISTOL_AMMO_COUNT } from './weapons/pistol.js';
import Shotgun from './weapons/shotgun.js';
import { SHOTGUN_AMMO_COUNT } from './weapons/shotgun.js';
import { DIRECTION_LEFT, DIRECTION_RIGHT } from './weapons/weapon.js';
import { BONUS_AMMO } from './bonuses/bonusAmmo';
import { BONUS_DAMAGE } from './bonuses/bonusDamage';
import { BONUS_HP } from './bonuses/bonusHP';
import { BONUS_JUMP } from './bonuses/bonusJump';
import { BONUS_SPEED } from './bonuses/bonusSpeed';
import { BONUS_PISTOL } from './bonuses/bonusPistol';
import { BONUS_SHOTGUN } from './bonuses/bonusShotgun';


export const VIEW_SIDE = {
    LEFT: "left",
    RIGHT: "right"
};

export default class Player extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, playerConfig, viewSide, x, y) {
        super(scene, x, y, 'dude');
        this.scene = scene;

        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.setBounce(0.2);
        this.body.pushable = false;
        this.setCollideWorldBounds(true);
        this.damageTakenSound = scene.sound.add('damage-taken');
        this.keys = this.mountControls(scene.input.keyboard, playerConfig.controls);

        this.Health = 100;
        this.name = playerConfig.name;
        this.viewSide = viewSide;
        this.weapon = new Pistol(scene);
        this.velocity = 200;
        this.jumpVelocity = -300;
    }

    HandleControls() {
        if (this.keys.left.isDown && !this.body.blocked.left) {
            this.setVelocityX(-1 * this.velocity);
            this.viewSide = VIEW_SIDE.LEFT;
            this.anims.play('left-move', true);
        } else if (this.keys.right.isDown && !this.body.blocked.right) {
            this.setVelocityX(this.velocity);
            this.viewSide = VIEW_SIDE.RIGHT;
            this.anims.play('right-move', true);
        } else {
            this.setVelocityX(0);
            this.body.blocked.left = false;
            this.body.blocked.right = false;
            switch (this.viewSide) {
                case VIEW_SIDE.LEFT:
                    this.anims.play('left-look');
                    break;
                case VIEW_SIDE.RIGHT:
                    this.anims.play('right-look');
                    break;
                default:
                    break;
            }
        }
        if (this.keys.up.isDown && this.body.touching.down) {
            this.setVelocityY(this.jumpVelocity);
        }

        if (this.keys.fire.isDown && !this.keys.fire.wasPressed) {
            const direction = (this.viewSide === VIEW_SIDE.LEFT) ? DIRECTION_LEFT : DIRECTION_RIGHT;
            this.weapon.Fire(this.x, this.y, direction);
            this.keys.fire.wasPressed = true;
        }
        if (this.keys.fire.isUp) {
            this.keys.fire.wasPressed = false;
        }
    }

    // make player (and his bullets) collide with other game objects  (colliders)
    AddColliders(colliders) {
        colliders.forEach(gameObj => {
            this.scene.physics.add.collider(this, gameObj);
            this.weapon.AddCollider(gameObj);
        });
    }

    // Player info that is showed at the top corner of the screen
    GetInfoText() {
        return [this.name, `Health: ${this.Health}`, `Weapon: ${this.weapon.Name}`, `Ammo: ${this.weapon.AmmoCount}`];
    }

    Collect(bonus) {
        switch (bonus.Type) {
            case BONUS_AMMO:
                this.weapon.AddAmmo(bonus.AdditionalAmmo);
                break;
            case BONUS_DAMAGE:
                this.weapon.IncreaseDamage(bonus.AdditionalDamage);
                break;
            case BONUS_HP:
                this.Health += bonus.AditionalHP;
                break;
            case BONUS_JUMP:
                this.jumpVelocity += bonus.AditionalJump;
                break;
            case BONUS_SPEED:
                this.velocity += bonus.AditionalSpeed;
                break;
            case BONUS_PISTOL:
                if (this.weapon.Name === BONUS_PISTOL) {
                    // already carying pistol - increase ammo
                    this.weapon.AddAmmo(PISTOL_AMMO_COUNT);
                } else {
                    this.weapon = new Pistol(this.scene, this.weapon);
                }
                break;
            case BONUS_SHOTGUN:
                if (this.weapon.Name === BONUS_SHOTGUN) {
                    // already caruing shotgun - increase ammo
                    this.weapon.AddAmmo(SHOTGUN_AMMO_COUNT);
                } else {
                    this.weapon = new Shotgun(this.scene, this.weapon);
                }
                break;
            default:
                console.log(`Unknown bonus type: ${bonus.Type}`);
                return;
        }
    }

    GetDamage(damage) {
        this.Health = Math.max(0, this.Health - damage);
        this.damageTakenSound.play();
    }

    mountControls(keyboard, controls) {
        return keyboard.addKeys({
            'left': controls.left,
            'right': controls.right,
            'up': controls.jump,
            'fire': controls.fire
        });
    }
}
