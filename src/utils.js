import 'regenerator-runtime/runtime'

const CONFIG_FILE_URL = "./src/gameConfig.json";

export async function LoadGameConfig() {
    return new Promise((resolve, reject) => {
        ReadJsonFile(CONFIG_FILE_URL).then(config => {
            resolve(config);
        }, errStatus => {
            alert(`Failed to load game config file, status: ${errStatus}`);
            reject();
        });
    });
}

export async function ReadJsonFile(fileUrl) {
    return new Promise((resolve, reject) => {
        const xhttp = new XMLHttpRequest();

        xhttp.open("GET", fileUrl, true);
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    resolve(JSON.parse(this.responseText));
                } else {
                    reject(this.status)
                }
            }
        }
        xhttp.send();
    });
}
