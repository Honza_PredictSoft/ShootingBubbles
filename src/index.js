import { LoadGameConfig } from "./utils.js";
import StartNewGame from './game.js';

const startButton = document.getElementById("start-game-button");
startButton.addEventListener("click", startGame);

const restartButton = document.getElementById("restart-game-button");
restartButton.addEventListener("click", restartGame);

var game;
var gameConfig;

// load and show the user controls into the main screen
LoadGameConfig().then(config => {
    gameConfig = config;

    const p1Left = document.getElementById("p1-left");
    p1Left.innerText += config.player1.controls.left;

    const p1Right = document.getElementById("p1-right");
    p1Right.innerText += config.player1.controls.right;

    const p1Jump = document.getElementById("p1-jump");
    p1Jump.innerText += config.player1.controls.jump;

    const p1Fire = document.getElementById("p1-fire");
    p1Fire.innerText += config.player1.controls.fire;


    const p2Left = document.getElementById("p2-left");
    p2Left.innerText += config.player2.controls.left;

    const p2Right = document.getElementById("p2-right");
    p2Right.innerText += config.player2.controls.right;

    const p2Jump = document.getElementById("p2-jump");
    p2Jump.innerText += config.player2.controls.jump;

    const p2Fire = document.getElementById("p2-fire");
    p2Fire.innerText += config.player2.controls.fire;
});


// lounch the Shooting bubbles game (run the Phaser)
function startGame() {
    if (!gameConfig) {
        // config file hasn't been loaded yet
        return;
    }

    const player1InfoBlock = document.getElementById("player1-info-block");
    const player2InfoBlock = document.getElementById("player2-info-block");

    player1InfoBlock.className += " hidden";
    player2InfoBlock.className += " hidden";
    startButton.className += " hidden";
    game = StartNewGame(gameConfig);
}

function restartGame() {
    restartButton.className += " hidden";
    // todo restart the game
}

export function showRestartButton() {
    restartButton.classList.remove("hidden");
}
