import Weapon from './weapon.js';
import { BONUS_PISTOL } from '../bonuses/bonusPistol.js';

export const PISTOL_AMMO_COUNT = 10;
const PISTOL_DAMAGE = 20;

export default class Pistol extends Weapon {
    constructor(scene, weapon=null) {
        var pistolSound = scene.sound.add('pistol-sound');
        super(scene, BONUS_PISTOL, PISTOL_AMMO_COUNT, PISTOL_DAMAGE, pistolSound, weapon);
    }
}