import Weapon from './weapon.js';
import { BulletGroup } from './weapon.js';
import { BONUS_SHOTGUN } from '../bonuses/bonusShotgun';


export const SHOTGUN_AMMO_COUNT = 10;
const SHOTGUN_DAMAGE = 10;

// Every shot fires 5 bullets
export default class Shotgun extends Weapon {
    constructor(scene, weapon=null) {
        var shotgunSound = scene.sound.add('shotgun-sound');
        super(scene, BONUS_SHOTGUN, SHOTGUN_AMMO_COUNT, SHOTGUN_DAMAGE, shotgunSound, weapon);
    }

    loadBullets() {
        this.bullets = new ShotgunBulletGroup(this.scene, this.AmmoCount);
        this.bullets.children.entries.forEach(bullet => {
            bullet.body.allowGravity = false;   // not very nice -> should be moved into Bullet constructor (somhow)
        });
    }

    Fire(x, y, direction) {
        if (this.AmmoCount > 0) {
            this.weaponSound.play();
            const bullets = this.bullets.FireShotgunBullet(x, y, direction);   // shotgun fires array of bullets
            this.bulletsColliders.forEach(collider => {
                bullets.forEach(bullet => {
                    this.scene.physics.add.overlap(bullet, collider, this.onBulletHit, null, this);
                });
            });
            this.AmmoCount -= 1;
        }
    }
}

class ShotgunBulletGroup extends BulletGroup
{
    constructor(scene, ammoCount) {
        super(scene, ammoCount * 5);    // 1 shot = 5 bullets
    }

    FireShotgunBullet(x, y, direction) {
        var bullet1 = this.FireBullet(x, y, direction, 200);
        var bullet2 = this.FireBullet(x, y, direction, 100);
        var bullet3 = this.FireBullet(x, y, direction, 0);
        var bullet4 = this.FireBullet(x, y, direction, -100);
        var bullet5 = this.FireBullet(x, y, direction, -200);

        return [bullet1, bullet2, bullet3, bullet4, bullet5];
    }
}