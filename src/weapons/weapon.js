export const DIRECTION_LEFT = "left";
export const DIRECTION_RIGHT = "right";

const BULLET_SPEED = 900;

export default class Weapon {
    constructor(scene, name, ammoCount, damage, sound, weapon=null) {
        this.scene = scene;
        this.Name = name;
        this.AmmoCount = ammoCount;
        this.damage = damage;
        this.weaponSound = sound;
        this.bulletsColliders = (weapon === null) ? [] : weapon.bulletsColliders; // objects that can collide with weapon shot
        this.loadBullets();
    }
    
    loadBullets() {
        this.bullets = new BulletGroup(this.scene, this.AmmoCount);
        this.bullets.children.entries.forEach(bullet => {
            bullet.body.allowGravity = false;   // not very nice -> should be moved into Bullet constructor (somehow) - todo
        });
    }

    Fire(x, y, direction) {
        if (this.AmmoCount > 0) {
            this.weaponSound.play();
            const bullet = this.bullets.FireBullet(x, y, direction);
            this.bulletsColliders.forEach(collider => {
                this.scene.physics.add.overlap(bullet, collider, this.onBulletHit, null, this);
            });
            this.AmmoCount -= 1;
        }
    }

    AddCollider(collider) {
        this.bulletsColliders.push(collider);
    }

    AddAmmo(ammoCount) {
        this.AmmoCount += ammoCount;
        this.loadBullets();
    }

    IncreaseDamage(damage) {
        this.damage += damage;
    }

    onBulletHit(bullet, gameObj) {
        if (gameObj.GetDamage) {
            gameObj.GetDamage(this.damage);
        }
        bullet.destroy(true);
        // todo add bullet explosion animation 
    }
}

export class BulletGroup extends Phaser.Physics.Arcade.Group
{
    constructor(scene, ammoCount) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: ammoCount,
            key: 'bullet',
            active: false,
            visible: false,
            classType: Bullet
        });
    }

    FireBullet(x, y, direction, uprise=0) {
        const bullet = this.getFirstDead(false);

        if(bullet) {
            bullet.Fire(x, y, direction, uprise);
        }
        return bullet;
    }
}

class Bullet extends Phaser.Physics.Arcade.Sprite
{
	constructor(scene, x, y) {
        super(scene, x, y, 'bullet');
        
        this.speed = BULLET_SPEED;
	}

	Fire(x, y, direction, uprise=0) {
		this.body.reset(x, y);

		this.setActive(true);
		this.setVisible(true);

        const velocity = (direction === DIRECTION_RIGHT) ? this.speed : (-1 * this.speed);
        this.setVelocity(velocity, uprise);
	}
}